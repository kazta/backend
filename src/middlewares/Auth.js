const { path } = require('../app');
const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    if (req.path !== '/api/auth/login') {
        if (req.headers.authorization) {
            const token = req.headers.authorization.split(' ')[1];
            jwt.verify(token, process.env.JWT_KEY, function (error, decoded) {
                if(error) return res.status(500).send({message: error});
                req.userId = decoded;
                next();
            });
        }
        else res.status(403).send({ message: "Missing token at header" });
    }
    else next();
}
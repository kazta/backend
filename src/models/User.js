const { Model, DataTypes } = require('sequelize');

const sequelize = require('../database');
const Role = require('./Role');

class User extends Model { }
User.init({
    email: DataTypes.STRING,
    password: DataTypes.TEXT,
    name: DataTypes.STRING,
    active: DataTypes.CHAR
}, { sequelize, modelName: "users" })

User.belongsTo(Role);

module.exports = User;

const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database');
const User = require('./User');

class Role extends Model { }
Role.init({
    name: DataTypes.STRING,
    description: DataTypes.STRING
}, { sequelize, modelName: "roles" })

module.exports = Role;

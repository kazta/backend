module.exports = (sequelize, Sequelize) => {
    const Candidate = sequelize.define("candidate", {
        name: { type: Sequelize.STRING },
        birthday: { type: Sequelize.DATE },
        experience: { type: Sequelize.INTEGER }
    });
    return Candidate;
};

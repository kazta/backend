const app = require('./app');

async function init(){
    await app.listen();
}

init();
const dbSetting = require('./connection');
const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(dbSetting.DB, dbSetting.USER, dbSetting.PASSWORD, {
    host: dbSetting.HOST,
    dialect: dbSetting.dialect
});

module.exports = sequelize;
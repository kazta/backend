require('dotenv').config();
var express = require('express');
var router = require('./routes');
const cors = require('cors');
const sequelize = require('./database')
const auth = require('./middlewares/Auth');
const { execSeeds } = require('./seeds');

//init
const app = express();
const config = {
    application: {
        cors: {
            server: [
                {
                    origin: "*",
                    credentials: true
                }
            ]
        }
    }
}

//server config
app.set('port', 80);
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors(config.application.cors.server))
app.use(auth);

//routes
app.use('/api', router);

app.listen(app.get('port'), async () => {
    console.log('Server run on port', app.get('port'));
    await sequelize.sync({ force: false });
    //insert min data for use app
    await execSeeds();
});

module.exports = app;
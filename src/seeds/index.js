const role = require('./roles');
const user = require('./users');

async function execSeeds(){
    await role.sync();
    await user.sync();
}

module.exports = {
    execSeeds
}
const { Role } = require('../models');

const migration = {};

migration.sync = async function () {
    const roles = await Role.findAll();
    if (roles.length === 0) {
        await Role.create({
            name: 'Administrador',
            description: 'Tiene permisos ilimitados para la administración del aplicativo'
        });

        await Role.create({
            name: 'Auditor',
            description: 'Tiene permisos para ver todos los modulos del aplicativo'
        });

        await Role.create({
            name: 'Auxiliar',
            description: 'Puede ver algunas partes del aplicativo, y tiene algunos permisos para crear y editar'
        });
    }
}

module.exports = migration;
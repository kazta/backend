const { User } = require('../models');

const migration = {};

migration.sync = async function () {
    const users = await User.findAll();
    if (users.length === 0) {
        //init bcryptjs
        const bcrypt = require('bcryptjs');
        const salt = await bcrypt.genSalt(5);
        await User.create({
            email: 'admin@back.com',
            password: await bcrypt.hash('admin', salt),
            name: 'Administardor',
            active: '1',
            roleId: 1
        });
    }
}

module.exports = migration;
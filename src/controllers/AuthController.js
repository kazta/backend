const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models');

exports.login = async (req, res) => {
    if (!req.body.email) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    // get user
    const user = {
        email: req.body.email,
        password: req.body.password
    };

    //select user
    const _user = await User.findOne({
        where: {
            email: user.email
        }
    });

    if (_user !== null && await bcryptjs.compare(user.password, _user.password)) {
        const token = jwt.sign(_user.id, process.env.JWT_KEY);
        res.send({
            auth: true,
            tokenType: 'Bearer',
            token: token
        })
    }
    else res.status(404).send({ message: 'User or password incorrect' });
};

exports.me = async (req, res) => {
    const id = req.userId;
    //select user
    const user = await User.findByPk(id, {
        attributes: ['email','name','active','roleId']
    });
    res.send({ user })
};
const Candidates = require('./CandidateController');
const Users = require('./UserController');
const Auth = require('./AuthController');
const Roles = require('./RoleController');

module.exports = {
    Auth,
    Candidates,
    Users,
    Roles
}
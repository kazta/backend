const { User, Role } = require('../models');

// add new user
exports.create = async (req, res) => {
    if (!req.body.email) {
        res.status(400).json({
            message: "Content can not be empty!"
        });
        return;
    }
    // init bcrypt
    const bcrypt = require('bcryptjs');
    const salt = await bcrypt.genSalt(5);
    // get user
    const _user = {
        email: req.body.email,
        password: await bcrypt.hash(req.body.password, salt),
        roleId: req.body.roleId,
        name: req.body.name,
        active: req.body.active
    };
    // save user
    const user = await User.create(_user);
    res.json(user)
};

// get all users
exports.findAll = async (req, res) => {
    const users = await User.findAll({ attributes: { exclude: ['password', 'roleId', 'updatedAt', 'createdAt'] }, include: { model: Role, as: 'role', attributes: { exclude: ['updatedAt', 'createdAt'] } } });
    res.json(users);
};

// get a user by id
exports.findOne = async (req, res) => {
    const id = req.params.id;
    const user = await User.findByPk(id);
    res.json(user);
};

// update a user by id
exports.update = async (req, res) => {
    const id = req.params.id;
    const user = await User.update(req.body, {
        where: { id: id }
    });
    res.json(user);
};

// delete a user by id
exports.delete = async (req, res) => {
    const id = req.params.id;
    const user = await User.destroy({
        where: { id: id }
    });
    res.json(user);
};

// delete all users
exports.deleteAll = (req, res) => {

};
const { Role } = require('../models');

// add new role
exports.create = async (req, res) => {
    if (!req.body.name) {
        res.status(400).json({
            message: "Content can not be empty!"
        });
        return;
    }
    // get role
    const _role = {
        name: req.body.name,
        description: req.body.description
    };
    // save role
    const role = await Role.create(_role);
    res.json(role)
};

// get all role
exports.findAll = async (req, res) => {
    const role = await Role.findAll();
    res.json(role);
};

// get a role by id
exports.findOne = async (req, res) => {
    const id = req.params.id;
    const role = await Role.findByPk(id);
    res.json(role);
};

// update a role by id
exports.update = async (req, res) => {
    const id = req.params.id;
    const role = await Role.update(req.body, {
        where: { id: id }
    });
    res.json(role);
};

// delete a role by id
exports.delete = async (req, res) => {
    const id = req.params.id;
    const role = await Role.destroy({
        where: { id: id }
    });
    res.json(role);
};

// delete all role
exports.deleteAll = (req, res) => {

};
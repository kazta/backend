const { Router } = require('express');
const { Auth, Candidates, Users, Roles } = require('./controllers');

var router = Router();

//Auth
router.post('/auth/login', Auth.login);
router.get('/auth/me', Auth.me);

//Candidates
router.get('/candidates', Candidates.findAll);
router.get('/candidates/:id', Candidates.findOne);
router.post('/candidates', Candidates.create);
router.put('/candidates/:id', Candidates.update);
router.delete('/candidates/:id', Candidates.delete);

//Users
router.get('/users', Users.findAll);
router.get('/users/:id', Users.findOne);
router.post('/users', Users.create);
router.put('/users/:id', Users.update);
router.delete('/users/:id', Users.delete);

//Roles
router.get('/roles', Roles.findAll);
router.get('/roles/:id', Roles.findOne);
router.post('/roles', Roles.create);
router.put('/roles/:id', Roles.update);
router.delete('/roles/:id', Roles.delete);

module.exports = router;